﻿using System;
using System.Configuration;
using System.ServiceModel;

namespace wcfConfig
{
    [ServiceContract(Namespace = "wcfConfig")]
    public class Calc
    {
        [OperationContract]
        double Sum(double x, double y)
        {
            return x + y;
        }
    }
}
