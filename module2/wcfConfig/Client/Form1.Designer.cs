﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xVal = new System.Windows.Forms.NumericUpDown();
            this.yVal = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.lbResult = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.xVal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yVal)).BeginInit();
            this.SuspendLayout();
            // 
            // xVal
            // 
            this.xVal.Location = new System.Drawing.Point(23, 21);
            this.xVal.Name = "xVal";
            this.xVal.Size = new System.Drawing.Size(120, 20);
            this.xVal.TabIndex = 0;
            // 
            // yVal
            // 
            this.yVal.Location = new System.Drawing.Point(23, 47);
            this.yVal.Name = "yVal";
            this.yVal.Size = new System.Drawing.Size(120, 20);
            this.yVal.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(185, 21);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbResult
            // 
            this.lbResult.AutoSize = true;
            this.lbResult.Location = new System.Drawing.Point(185, 51);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(0, 13);
            this.lbResult.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 122);
            this.Controls.Add(this.lbResult);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.yVal);
            this.Controls.Add(this.xVal);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.xVal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yVal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown xVal;
        private System.Windows.Forms.NumericUpDown yVal;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbResult;
    }
}

