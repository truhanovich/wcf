﻿using System;
using System.Windows.Forms;

namespace Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var x = (double)xVal.Value;
            var y = (double)yVal.Value;

            var client = new WCFConfig.CalcClient("NetTcpBinding_Calc");

            lbResult.Text = client.Sum(x, y).ToString();
        }
    }
}
