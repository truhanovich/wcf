﻿using System;
using System.ServiceModel;

namespace WCFConfigHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var host = new ServiceHost(typeof(wcfConfig.Calc), new Uri("http://localhost:8080/")))
            {
                host.Open();

                Console.WriteLine("Host started");
                Console.ReadLine();
            }
        }
    }
}
