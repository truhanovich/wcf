﻿using System;
using System.Security.Permissions;
using System.Security.Principal;

namespace SecurityPrincipa
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
            PrincipalPermission principalPerm = new PrincipalPermission(null, "Administrators");
            try
            {
                principalPerm.Demand();
                Console.WriteLine("Demand succeeded.");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
