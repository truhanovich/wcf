﻿using System;
using System.IO;
using System.Security.Permissions;

namespace Permissions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MyClass.Method();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }

    [FileIOPermission(SecurityAction.Assert, Read = "C:/")]
    class MyClass
    {
        public static void Method()
        {
            File.WriteAllText("C:/1.txt", "Test");
        }
    }
}
