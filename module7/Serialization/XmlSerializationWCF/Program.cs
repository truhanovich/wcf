﻿using System;

namespace XmlSerializationWCF
{
    class Program
    {
        static void Main(string[] args)
        {
            var workWithXml = new WorkWithXml();

            workWithXml.CreatePO("po.xml");
            workWithXml.ReadPO("po.xml");

            Console.ReadLine();
        }
    }
}
