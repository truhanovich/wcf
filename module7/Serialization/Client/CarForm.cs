﻿using Client.CarServiceReference;
using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Windows.Forms;

namespace Client
{
    public partial class CarForm : Form
    {
        private string bindingType;
        CarServiceClient _client;

        public CarForm()
        {
            InitializeComponent();
            var clientSection = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
            var endpointCollection = clientSection.ElementInformation.Properties[string.Empty].Value as ChannelEndpointElementCollection;
            bindingType = endpointCollection[0].Name;
            _client = new CarServiceClient(bindingType);
        }

        private void button1_Click(object sender, EventArgs e)
        {
             if(_client.State == CommunicationState.Faulted) { _client = new CarServiceClient(bindingType); }

             if(setRB.Checked)
            {
                SetCar();
            }
            else
            {
                GetCar();
            }
        }

        private void setRB_CheckedChanged(object sender, EventArgs e)
        {
            vendorTb.Enabled = setRB.Checked;
            modelTb.Enabled = setRB.Checked;
            YearTb.Enabled = setRB.Checked;
            mileageTb.Enabled = setRB.Checked;

            actionBt.Text = setRB.Checked ? "Set car" : "Get car";
        }

        private void GetCar()
        {
            Car car;

            try
            {
                car = _client.GetCar(int.Parse(idTb.Text));
            }
            catch (FaultException ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                vendorTb.Text = string.Empty;
                modelTb.Text = string.Empty;
                mileageTb.Text = string.Empty;
                YearTb.Text = string.Empty;
                return;
            }

            vendorTb.Text = car.Vendor;
            modelTb.Text = car.Model;
            mileageTb.Text = car.Mileage.ToString();
            YearTb.Text = car.Year.ToString("yyyy");
        }

        private void SetCar()
        {
            Car car = new Car
            {
                Id = int.Parse(idTb.Text),
                Vendor = vendorTb.Text,
                Model = modelTb.Text,
                Mileage = int.Parse(mileageTb.Text),
                Year = new DateTime(int.Parse(YearTb.Text), 1, 1)
            };

            _client.SetCar(car);
        }
    }
}
