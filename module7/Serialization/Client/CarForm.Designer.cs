﻿using System;

namespace Client
{
    partial class CarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.actionBt = new System.Windows.Forms.Button();
            this.idLb = new System.Windows.Forms.Label();
            this.vendorLb = new System.Windows.Forms.Label();
            this.modelLb = new System.Windows.Forms.Label();
            this.yearLb = new System.Windows.Forms.Label();
            this.idTb = new System.Windows.Forms.TextBox();
            this.vendorTb = new System.Windows.Forms.TextBox();
            this.modelTb = new System.Windows.Forms.TextBox();
            this.YearTb = new System.Windows.Forms.TextBox();
            this.mileageTb = new System.Windows.Forms.TextBox();
            this.mileageLb = new System.Windows.Forms.Label();
            this.getRB = new System.Windows.Forms.RadioButton();
            this.setRB = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // actionBt
            // 
            this.actionBt.Location = new System.Drawing.Point(112, 187);
            this.actionBt.Name = "actionBt";
            this.actionBt.Size = new System.Drawing.Size(75, 23);
            this.actionBt.TabIndex = 0;
            this.actionBt.Text = "Get Car";
            this.actionBt.UseVisualStyleBackColor = true;
            this.actionBt.Click += new System.EventHandler(this.button1_Click);
            // 
            // idLb
            // 
            this.idLb.AutoSize = true;
            this.idLb.Location = new System.Drawing.Point(34, 46);
            this.idLb.Name = "idLb";
            this.idLb.Size = new System.Drawing.Size(19, 13);
            this.idLb.TabIndex = 1;
            this.idLb.Text = "Id:";
            // 
            // vendorLb
            // 
            this.vendorLb.AutoSize = true;
            this.vendorLb.Location = new System.Drawing.Point(34, 75);
            this.vendorLb.Name = "vendorLb";
            this.vendorLb.Size = new System.Drawing.Size(44, 13);
            this.vendorLb.TabIndex = 2;
            this.vendorLb.Text = "Vendor:";
            // 
            // modelLb
            // 
            this.modelLb.AutoSize = true;
            this.modelLb.Location = new System.Drawing.Point(34, 101);
            this.modelLb.Name = "modelLb";
            this.modelLb.Size = new System.Drawing.Size(39, 13);
            this.modelLb.TabIndex = 3;
            this.modelLb.Text = "Model:";
            // 
            // yearLb
            // 
            this.yearLb.AutoSize = true;
            this.yearLb.Location = new System.Drawing.Point(34, 131);
            this.yearLb.Name = "yearLb";
            this.yearLb.Size = new System.Drawing.Size(32, 13);
            this.yearLb.TabIndex = 4;
            this.yearLb.Text = "Year:";
            // 
            // idTb
            // 
            this.idTb.Location = new System.Drawing.Point(87, 46);
            this.idTb.Name = "idTb";
            this.idTb.Size = new System.Drawing.Size(100, 20);
            this.idTb.TabIndex = 5;
            // 
            // vendorTb
            // 
            this.vendorTb.Enabled = false;
            this.vendorTb.Location = new System.Drawing.Point(87, 72);
            this.vendorTb.Name = "vendorTb";
            this.vendorTb.Size = new System.Drawing.Size(100, 20);
            this.vendorTb.TabIndex = 6;
            // 
            // modelTb
            // 
            this.modelTb.Enabled = false;
            this.modelTb.Location = new System.Drawing.Point(87, 98);
            this.modelTb.Name = "modelTb";
            this.modelTb.Size = new System.Drawing.Size(100, 20);
            this.modelTb.TabIndex = 7;
            // 
            // YearTb
            // 
            this.YearTb.Enabled = false;
            this.YearTb.Location = new System.Drawing.Point(87, 124);
            this.YearTb.Name = "YearTb";
            this.YearTb.Size = new System.Drawing.Size(100, 20);
            this.YearTb.TabIndex = 8;
            // 
            // mileageTb
            // 
            this.mileageTb.Enabled = false;
            this.mileageTb.Location = new System.Drawing.Point(87, 150);
            this.mileageTb.Name = "mileageTb";
            this.mileageTb.Size = new System.Drawing.Size(100, 20);
            this.mileageTb.TabIndex = 10;
            // 
            // mileageLb
            // 
            this.mileageLb.AutoSize = true;
            this.mileageLb.Location = new System.Drawing.Point(34, 157);
            this.mileageLb.Name = "mileageLb";
            this.mileageLb.Size = new System.Drawing.Size(47, 13);
            this.mileageLb.TabIndex = 9;
            this.mileageLb.Text = "Mileage:";
            // 
            // getRB
            // 
            this.getRB.AutoSize = true;
            this.getRB.Checked = true;
            this.getRB.Location = new System.Drawing.Point(25, 13);
            this.getRB.Name = "getRB";
            this.getRB.Size = new System.Drawing.Size(42, 17);
            this.getRB.TabIndex = 11;
            this.getRB.TabStop = true;
            this.getRB.Text = "Get";
            this.getRB.UseVisualStyleBackColor = true;
            // 
            // setRB
            // 
            this.setRB.AutoSize = true;
            this.setRB.Location = new System.Drawing.Point(87, 13);
            this.setRB.Name = "setRB";
            this.setRB.Size = new System.Drawing.Size(41, 17);
            this.setRB.TabIndex = 12;
            this.setRB.Text = "Set";
            this.setRB.UseVisualStyleBackColor = true;
            this.setRB.CheckedChanged += new System.EventHandler(this.setRB_CheckedChanged);
            // 
            // CarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 222);
            this.Controls.Add(this.setRB);
            this.Controls.Add(this.getRB);
            this.Controls.Add(this.mileageTb);
            this.Controls.Add(this.mileageLb);
            this.Controls.Add(this.YearTb);
            this.Controls.Add(this.modelTb);
            this.Controls.Add(this.vendorTb);
            this.Controls.Add(this.idTb);
            this.Controls.Add(this.yearLb);
            this.Controls.Add(this.modelLb);
            this.Controls.Add(this.vendorLb);
            this.Controls.Add(this.idLb);
            this.Controls.Add(this.actionBt);
            this.Name = "CarForm";
            this.Text = "Car form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.Button actionBt;
        private System.Windows.Forms.Label idLb;
        private System.Windows.Forms.Label vendorLb;
        private System.Windows.Forms.Label modelLb;
        private System.Windows.Forms.Label yearLb;
        private System.Windows.Forms.TextBox idTb;
        private System.Windows.Forms.TextBox vendorTb;
        private System.Windows.Forms.TextBox modelTb;
        private System.Windows.Forms.TextBox YearTb;
        private System.Windows.Forms.TextBox mileageTb;
        private System.Windows.Forms.Label mileageLb;
        private System.Windows.Forms.RadioButton getRB;
        private System.Windows.Forms.RadioButton setRB;
    }
}