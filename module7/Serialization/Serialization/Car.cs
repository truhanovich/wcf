﻿using System;
using System.Runtime.Serialization;

namespace Serialization
{
    public class Car
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Vendor { get; set; }
        [DataMember]
        public string Model { get; set; }
        [DataMember]
        public DateTime Year { get; set; }
        [DataMember]
        public int Mileage { get; set; }
    }
}
