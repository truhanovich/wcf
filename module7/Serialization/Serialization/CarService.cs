﻿using System;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;

namespace Serialization
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CarService" in both code and config file together.
    public class CarService : ICarService
    {
        public Car GetCar(int id)
        {
            var file = ConfigurationManager.AppSettings["fileCar"];
            var doc = XDocument.Load(file);

            var result=  doc.Descendants("Car").FirstOrDefault(c => c.Attribute("Id").Value == id.ToString());

            return new Car
            {
                Id = int.Parse(result.Attribute("Id").Value),
                Vendor = result.Element("Vendor").Value,
                Mileage = int.Parse(result.Element("Mileage").Value),
                Model = result.Element("Model").Value,
                Year = new DateTime(int.Parse(result.Element("Year").Value), 1, 1)
            };
        }

        public void SetCar(Car c)
        {
            var file = ConfigurationManager.AppSettings["fileCar"];
            var doc = XDocument.Load(file);
            doc.Root.Add(new XElement("Car", new XAttribute("Id", c.Id),
                new XElement("Vendor", c.Vendor),
                new XElement("Model", c.Model),
                new XElement("Year", c.Year.Year),
                new XElement("Mileage", c.Mileage)
                ));

            doc.Save(file);
        }
    }
}
