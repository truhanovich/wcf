﻿using System.ServiceModel;

namespace Serialization
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICarService" in both code and config file together.
    [ServiceContract]
    public interface ICarService
    {
        [OperationContract]
        [XmlSerializerFormat]
        void SetCar(Car c);

        [OperationContract]
        [XmlSerializerFormat]
        Car GetCar(int id);
    }
}
