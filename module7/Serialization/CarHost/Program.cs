﻿using System;

namespace CarHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var host = new System.ServiceModel.ServiceHost(typeof(Serialization.CarService)))
            {
                host.Open();

                Console.WriteLine("Host started");
                Console.Title = "Server";
                Console.ReadLine();
            }
        }
    }
}
