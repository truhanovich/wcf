﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace BehaviorService
{
    public class MyServiceBehaviorAttribute : Attribute, IServiceBehavior
    {
        // implementation of IServiceBehavior interface
        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
            return;
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            Console.WriteLine("The EnforceGreetingFaultBehavior has been applied.");
            foreach (ChannelDispatcher chanDisp in serviceHostBase.ChannelDispatchers)
            {
                Console.WriteLine(chanDisp.ToString());
            }
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            Console.WriteLine("Validate is called.");
            foreach (ServiceEndpoint se in serviceDescription.Endpoints)
            {
                // Must not examine any metadata endpoint.
                if (se.Contract.Name.Equals("IMetadataExchange"))
                    continue;

                foreach (OperationDescription opDesc in se.Contract.Operations)
                {
                    if (opDesc.Faults.Count == 0)
                        throw new InvalidOperationException(String.Format(
                          "EnforceGreetingFaultBehavior requires a "
                          + "FaultContractAttribute(typeof(GreetingFault)) in each operation contract.  "
                          + "The \"{0}\" operation contains no FaultContractAttribute.",
                          opDesc.Name)
                        );

                    bool gfExists = false;
                    foreach (FaultDescription fault in opDesc.Faults)
                    {
                        if (fault.DetailType.Equals(typeof(GreetingFault)))
                        {
                            gfExists = true;
                            continue;
                        }
                    }

                    if (gfExists == false)
                    {
                        throw new InvalidOperationException("EnforceGreetingFaultBehavior requires a FaultContractAttribute(typeof(GreetingFault)) in an operation contract.");
                    }
                }
            }
        }
    }
}
