﻿using System.Runtime.Serialization;

namespace BehaviorService
{
    [DataContractAttribute]
    public class GreetingFault
    {
        private string report = "default message";
        
        public GreetingFault(string message)
        {
            this.report = message;
        }

        [DataMemberAttribute]
        public string Message
        {
            get { return this.report; }
            set { this.report = value; }
        }
    }
}
