﻿using System.Net.Security;
using System.ServiceModel;

namespace BehaviorService
{
    [MyServiceBehaviorAttribute]
    [ServiceContract]
    public interface IService
    {
        [MyServiceBehaviorAttribute]
        [OperationContract]
        [FaultContractAttribute(typeof(GreetingFault), Action = "http://localhost:8080/", ProtectionLevel = ProtectionLevel.None)]
        string GetData(int value);
    }
}
