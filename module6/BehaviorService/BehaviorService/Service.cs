﻿using System;

namespace BehaviorService
{
    [MyServiceBehavior]
    public class Service : IService
    {
        public string GetData(int value)
        {
            Console.WriteLine(value);
            return string.Format("You entered: {0}", value);
        }
    }
}
