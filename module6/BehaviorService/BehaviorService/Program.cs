﻿using System;
using System.ServiceModel;

namespace BehaviorService
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var host = new ServiceHost(typeof(Service), new Uri("http://localhost:8080/")))
            {
                host.Open();

                Console.WriteLine("Host started");
                Console.ReadLine();
            }
        }
    }
}
