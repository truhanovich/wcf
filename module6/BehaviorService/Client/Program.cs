﻿using System;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new ServiceReference1.ServiceClient("NetTcpBinding_IService");

            var result  = client.GetData(2).ToString();
            Console.WriteLine(result);

            Console.ReadLine();
        }
    }
}
