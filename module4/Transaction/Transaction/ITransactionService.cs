﻿using System.ServiceModel;

namespace Transaction
{
    [ServiceContract]
    public interface ITransactionService
    {
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void Update(int updateValue);

        [OperationContract]
        int Get();

    }
}
