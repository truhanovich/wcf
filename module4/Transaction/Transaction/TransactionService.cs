﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;

namespace Transaction
{
    public class TransactionService : ITransactionService
    {
        readonly private string _format = "yyyy/MM/dd hh:mm";
        readonly string _valuePath = "Data\\Value.txt";
        readonly string _historyPath = "Data\\History.txt";

        public int Get()
        {
            var result = File.ReadLines(_valuePath).FirstOrDefault();
            return int.Parse(string.IsNullOrEmpty(result) ? "0" : result);
        }

        [OperationBehavior(TransactionScopeRequired = true)]
        public void Update(int updateValue)
        {
            try
            {
                UpdateFile(updateValue);
            }

            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw new FaultException(ex.Message);
            }
        }

        private void UpdateFile(int updateValue)
        {
            var result = File.ReadLines(_valuePath).FirstOrDefault();
            var old = int.Parse(string.IsNullOrEmpty(result) ? "0" : result);

            File.AppendAllText(_historyPath, $"Old value {old}: {DateTime.Now.ToString(_format)} {Environment.NewLine}");
            var newVal = old + updateValue;
            File.WriteAllText(_valuePath, newVal.ToString());
            throw new Exception();
            File.AppendAllText(_historyPath, $"New value {newVal}: {DateTime.Now.ToString(_format)} {Environment.NewLine}");
        }
    }
}
