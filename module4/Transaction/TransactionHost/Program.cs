﻿using System;

namespace TransactionHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var host = new System.ServiceModel.ServiceHost(typeof(Transaction.TransactionService)))
            {
                host.Open();

                Console.WriteLine("Host started");
                Console.Title = "Server";
                Console.ReadLine();
            }
        }
    }
}
