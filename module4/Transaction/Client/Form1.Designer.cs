﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.withTransaction = new System.Windows.Forms.RadioButton();
            this.withoutTransaction = new System.Windows.Forms.RadioButton();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.getBt = new System.Windows.Forms.Button();
            this.updateBt = new System.Windows.Forms.Button();
            this.getLb = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // withTransaction
            // 
            this.withTransaction.AutoSize = true;
            this.withTransaction.Location = new System.Drawing.Point(27, 38);
            this.withTransaction.Name = "withTransaction";
            this.withTransaction.Size = new System.Drawing.Size(106, 17);
            this.withTransaction.TabIndex = 0;
            this.withTransaction.TabStop = true;
            this.withTransaction.Text = "With Transaction";
            this.withTransaction.UseVisualStyleBackColor = true;
            // 
            // withoutTransaction
            // 
            this.withoutTransaction.AutoSize = true;
            this.withoutTransaction.Location = new System.Drawing.Point(27, 62);
            this.withoutTransaction.Name = "withoutTransaction";
            this.withoutTransaction.Size = new System.Drawing.Size(117, 17);
            this.withoutTransaction.TabIndex = 1;
            this.withoutTransaction.TabStop = true;
            this.withoutTransaction.Text = "Without transaction";
            this.withoutTransaction.UseVisualStyleBackColor = true;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(27, 86);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(93, 20);
            this.numericUpDown1.TabIndex = 2;
            // 
            // getBt
            // 
            this.getBt.Location = new System.Drawing.Point(148, 38);
            this.getBt.Name = "getBt";
            this.getBt.Size = new System.Drawing.Size(75, 23);
            this.getBt.TabIndex = 3;
            this.getBt.Text = "Get";
            this.getBt.UseVisualStyleBackColor = true;
            this.getBt.Click += new System.EventHandler(this.getBt_Click);
            // 
            // updateBt
            // 
            this.updateBt.Location = new System.Drawing.Point(27, 112);
            this.updateBt.Name = "updateBt";
            this.updateBt.Size = new System.Drawing.Size(75, 23);
            this.updateBt.TabIndex = 4;
            this.updateBt.Text = "Update";
            this.updateBt.UseVisualStyleBackColor = true;
            this.updateBt.Click += new System.EventHandler(this.updateBt_Click);
            // 
            // getLb
            // 
            this.getLb.AutoSize = true;
            this.getLb.Location = new System.Drawing.Point(150, 66);
            this.getLb.Name = "getLb";
            this.getLb.Size = new System.Drawing.Size(0, 13);
            this.getLb.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 310);
            this.Controls.Add(this.getLb);
            this.Controls.Add(this.updateBt);
            this.Controls.Add(this.getBt);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.withoutTransaction);
            this.Controls.Add(this.withTransaction);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton withTransaction;
        private System.Windows.Forms.RadioButton withoutTransaction;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button getBt;
        private System.Windows.Forms.Button updateBt;
        private System.Windows.Forms.Label getLb;
    }
}

