﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Transactions;
using System.Windows.Forms;
using Client.ServiceTransaction;

namespace Client
{
    public partial class Form1 : Form
    {
        private string bindingType;
        TransactionServiceClient _client;
        public Form1()
        {
            InitializeComponent();
            var clientSection = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
            var endpointCollection = clientSection.ElementInformation.Properties[string.Empty].Value as ChannelEndpointElementCollection;
            bindingType = endpointCollection[0].Name;
            _client = new TransactionServiceClient(bindingType);
        }

        private void getBt_Click(object sender, EventArgs e)
        {
            if (_client.State == CommunicationState.Faulted) { _client = new TransactionServiceClient(bindingType); }
            getLb.Text = _client.Get().ToString();
        }

        private void updateBt_Click(object sender, EventArgs e)
        {
            if (_client.State == CommunicationState.Faulted) { _client = new TransactionServiceClient(bindingType); }

            if (withTransaction.Checked)
            {
                using (var transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    _client.Update((int)numericUpDown1.Value);
                    transactionScope.Complete();
                }
                
            }
            else
            {
                _client.Update((int)numericUpDown1.Value);
            }
        }
    }
}
