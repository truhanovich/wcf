﻿using System.ServiceModel;

namespace MessageQueuingService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMessageQueuingService" in both code and config file together.
    [ServiceContract(Namespace = "MessageQueuingService")]
    [ServiceKnownType(typeof(PurchaseOrder))]
    public interface IMessageQueuingService
    {
        [OperationContract(IsOneWay = true)]
        void SubmitPurchaseOrder(PurchaseOrder po);
    }
}
