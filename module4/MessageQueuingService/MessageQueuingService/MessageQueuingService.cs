﻿using System;
using System.ServiceModel;

namespace MessageQueuingService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MessageQueuingService" in both code and config file together.
    public class MessageQueuingService : IMessageQueuingService
    {
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        public void SubmitPurchaseOrder(PurchaseOrder po)
        {
            Orders.Add(po);
            Console.WriteLine("Processing {0} ", po);
        }
    }
}
