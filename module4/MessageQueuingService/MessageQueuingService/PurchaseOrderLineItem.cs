﻿using System;
using System.Runtime.Serialization;

namespace MessageQueuingService
{
    // Define the purchase order line item.
    [DataContract(Namespace = "MessageQueuingService")]
    public class PurchaseOrderLineItem
    {
        [DataMember]
        public string ProductId;

        [DataMember]
        public float UnitCost;

        [DataMember]
        public int Quantity;

        public override string ToString() => $"Order LineItem: {Quantity} of {ProductId} @unit price: ${UnitCost}\n";

        public float TotalCost => UnitCost * Quantity;
    }
}
