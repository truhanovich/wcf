﻿using System;
using System.ServiceModel;

namespace Server
{
    class Program
    {
        static void Main()
        {
            Console.Title = "SERVER";
            var address = new Uri("http://localhost:7675/IContract");
            var binding = new BasicHttpBinding();
            var contract = typeof(IService);

            var host = new ServiceHost(typeof(Service));

            host.AddServiceEndpoint(contract, binding, address);

            host.Open();

            Console.WriteLine("Server started");
            Console.ReadLine();
        }
    }
}
