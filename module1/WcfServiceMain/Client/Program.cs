﻿using System;
using System.ServiceModel;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Client";
            var address = new Uri("http://localhost:7675/IContract");
            var binding = new BasicHttpBinding();

            var endpoint = new EndpointAddress(address);

            var factory = new ChannelFactory<IService>(binding, endpoint);

            var channel = factory.CreateChannel();

            var result = channel.GetData(1);

            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
