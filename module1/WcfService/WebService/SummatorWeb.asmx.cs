﻿using System.Web.Services;

namespace WebService
{
    /// <summary>
    /// Summary description for SummatorWeb
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SummatorWeb : System.Web.Services.WebService
    {

        [WebMethod]
        public int GetSumm(int x, int y)
        {
            return x + y;
        }
    }
}
