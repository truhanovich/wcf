﻿using System.Windows;
using WpfWebService.SummatorWebService;

namespace WpfWebService
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var clietn = new SummatorWebSoapClient();

            lbResult.Content = clietn.GetSumm(int.Parse(xVal.Text), int.Parse(yVal.Text)).ToString();
        }
    }
}
