﻿using System;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using SummatorRemoteService;

namespace SummatorClient
{
    public partial class Form1 : Form
    {
        private ISummatorRemoteService _instance;
        public Form1()
        {
            InitializeComponent();

            var channel = new TcpChannel();
            ChannelServices.RegisterChannel(channel, false);

            _instance = (ISummatorRemoteService)Activator.GetObject(typeof(ISummatorRemoteService), "tcp://localhost:43434/Summator");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            result.Text = _instance.GetSumm((int)xVal.Value, (int)yVal.Value).ToString();
        }
    }
}
