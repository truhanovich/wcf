﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummatorRemoteService
{
    public interface ISummatorRemoteService
    {
        int GetSumm(int x, int y);
    }
}
