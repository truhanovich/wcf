﻿using System;

namespace SummatorRemoteService
{
    public class SummatorRemoteService : MarshalByRefObject, ISummatorRemoteService
    {
        public int GetSumm(int x, int y)
        {
            return x + y;
        }
    }
}
