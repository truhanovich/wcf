﻿using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

namespace SummatorHost
{
    class Program
    {
        static void Main()
        {
            var chanal = new TcpChannel(43434);
            ChannelServices.RegisterChannel(chanal, false);

            var service = new SummatorRemoteService.SummatorRemoteService();

            RemotingConfiguration.RegisterWellKnownServiceType(typeof(SummatorRemoteService.SummatorRemoteService),
                "Summator", WellKnownObjectMode.SingleCall);

            Console.WriteLine("Service started");
            Console.ReadLine();
        }
    }
}
