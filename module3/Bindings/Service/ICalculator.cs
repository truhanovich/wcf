﻿using System.ServiceModel;

namespace Service
{
    // Define a service contract for the calculator. 
    [ServiceContract()]
    public interface ICalculator
    {
        [OperationContract()]
        double Add(double n1, double n2);
    }
}
