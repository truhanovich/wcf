﻿using System;
using System.Configuration;
using System.ServiceModel.Configuration;
using System.Windows.Forms;
using Client.ServiceCalc;

namespace Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            var clientSection = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
            var endpointCollection = clientSection.ElementInformation.Properties[string.Empty].Value as ChannelEndpointElementCollection;

            foreach (ChannelEndpointElement endpointElement in endpointCollection)
            {
                bindingType.Items.Add(endpointElement.Name);
            }
            
            bindingType.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var x = (double)xVal.Value;
            var y = (double)yVal.Value;

            var client = new CalculatorClient(bindingType.SelectedItem.ToString());

            result.Text = client.Add(x, y).ToString();
        }
    }
}
