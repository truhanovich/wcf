﻿using System;

namespace ServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var host = new System.ServiceModel.ServiceHost(typeof(Service.Calculator)))
            {
                host.Open();

                Console.WriteLine("Host started");
                Console.ReadLine();
            }
        }
    }
}
