﻿using System;
using System.Linq;
using System.ServiceModel;

namespace Session
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CarService" in both code and config file together.
    public class CarService : ICarService
    {
        public Car GetCar(int id)
        {
            var cars = new CarFactory().Build();
            Car car = null;
            try
            {
                car = cars.Single(c => c.Id == id);
            }
            catch(Exception ex)
            {
                throw new FaultException(ex.Message);
            }

            return car;
        }
    }
}
