﻿using System;
using System.Collections.Generic;

namespace Session
{
    internal class CarFactory
    {
        private readonly List<Car> _cars = new List<Car>() {new Car {Id =12, Model = "312", Year = new DateTime(1999, 11, 12), Vendor = "BMV" },
            new Car {Id = 15, Model= "Vesta", Year = new DateTime(2015, 10, 21), Vendor= "Lada", Mileage = 1288},
            new Car{Id = 24, Model = "S90", Year = new DateTime(2017, 1,5), Vendor= "Volvo", Mileage = 24335} };

        internal List<Car> Build()
        {
            return _cars;
        }
    }
}
