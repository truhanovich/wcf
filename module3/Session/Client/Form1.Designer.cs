﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.idLb = new System.Windows.Forms.Label();
            this.vendorLb = new System.Windows.Forms.Label();
            this.modelLb = new System.Windows.Forms.Label();
            this.yearLb = new System.Windows.Forms.Label();
            this.idTb = new System.Windows.Forms.TextBox();
            this.vendorTb = new System.Windows.Forms.TextBox();
            this.modelTb = new System.Windows.Forms.TextBox();
            this.YearTb = new System.Windows.Forms.TextBox();
            this.mileageTb = new System.Windows.Forms.TextBox();
            this.mileageLb = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(112, 187);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Get Car";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // idLb
            // 
            this.idLb.AutoSize = true;
            this.idLb.Location = new System.Drawing.Point(34, 46);
            this.idLb.Name = "idLb";
            this.idLb.Size = new System.Drawing.Size(19, 13);
            this.idLb.TabIndex = 1;
            this.idLb.Text = "Id:";
            // 
            // vendorLb
            // 
            this.vendorLb.AutoSize = true;
            this.vendorLb.Location = new System.Drawing.Point(34, 75);
            this.vendorLb.Name = "vendorLb";
            this.vendorLb.Size = new System.Drawing.Size(44, 13);
            this.vendorLb.TabIndex = 2;
            this.vendorLb.Text = "Vendor:";
            // 
            // modelLb
            // 
            this.modelLb.AutoSize = true;
            this.modelLb.Location = new System.Drawing.Point(34, 101);
            this.modelLb.Name = "modelLb";
            this.modelLb.Size = new System.Drawing.Size(39, 13);
            this.modelLb.TabIndex = 3;
            this.modelLb.Text = "Model:";
            // 
            // yearLb
            // 
            this.yearLb.AutoSize = true;
            this.yearLb.Location = new System.Drawing.Point(34, 131);
            this.yearLb.Name = "yearLb";
            this.yearLb.Size = new System.Drawing.Size(32, 13);
            this.yearLb.TabIndex = 4;
            this.yearLb.Text = "Year:";
            // 
            // idTb
            // 
            this.idTb.Location = new System.Drawing.Point(87, 46);
            this.idTb.Name = "idTb";
            this.idTb.Size = new System.Drawing.Size(100, 20);
            this.idTb.TabIndex = 5;
            // 
            // vendorTb
            // 
            this.vendorTb.Enabled = false;
            this.vendorTb.Location = new System.Drawing.Point(87, 72);
            this.vendorTb.Name = "vendorTb";
            this.vendorTb.Size = new System.Drawing.Size(100, 20);
            this.vendorTb.TabIndex = 6;
            // 
            // modelTb
            // 
            this.modelTb.Enabled = false;
            this.modelTb.Location = new System.Drawing.Point(87, 98);
            this.modelTb.Name = "modelTb";
            this.modelTb.Size = new System.Drawing.Size(100, 20);
            this.modelTb.TabIndex = 7;
            // 
            // YearTb
            // 
            this.YearTb.Enabled = false;
            this.YearTb.Location = new System.Drawing.Point(87, 124);
            this.YearTb.Name = "YearTb";
            this.YearTb.Size = new System.Drawing.Size(100, 20);
            this.YearTb.TabIndex = 8;
            // 
            // mileageTb
            // 
            this.mileageTb.Enabled = false;
            this.mileageTb.Location = new System.Drawing.Point(87, 150);
            this.mileageTb.Name = "mileageTb";
            this.mileageTb.Size = new System.Drawing.Size(100, 20);
            this.mileageTb.TabIndex = 10;
            // 
            // mileageLb
            // 
            this.mileageLb.AutoSize = true;
            this.mileageLb.Location = new System.Drawing.Point(34, 157);
            this.mileageLb.Name = "mileageLb";
            this.mileageLb.Size = new System.Drawing.Size(47, 13);
            this.mileageLb.TabIndex = 9;
            this.mileageLb.Text = "Mileage:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 222);
            this.Controls.Add(this.mileageTb);
            this.Controls.Add(this.mileageLb);
            this.Controls.Add(this.YearTb);
            this.Controls.Add(this.modelTb);
            this.Controls.Add(this.vendorTb);
            this.Controls.Add(this.idTb);
            this.Controls.Add(this.yearLb);
            this.Controls.Add(this.modelLb);
            this.Controls.Add(this.vendorLb);
            this.Controls.Add(this.idLb);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label idLb;
        private System.Windows.Forms.Label vendorLb;
        private System.Windows.Forms.Label modelLb;
        private System.Windows.Forms.Label yearLb;
        private System.Windows.Forms.TextBox idTb;
        private System.Windows.Forms.TextBox vendorTb;
        private System.Windows.Forms.TextBox modelTb;
        private System.Windows.Forms.TextBox YearTb;
        private System.Windows.Forms.TextBox mileageTb;
        private System.Windows.Forms.Label mileageLb;
    }
}

