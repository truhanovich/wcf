﻿using System;
using System.Configuration;
using System.ServiceModel.Configuration;
using System.Windows.Forms;
using Client.CarService;
using System.ServiceModel;

namespace Client
{
    
    public partial class Form1 : Form
    {
        private string bindingType;
        CarServiceClient _client;
        public Form1()
        {
            InitializeComponent();
            var clientSection = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
            var endpointCollection = clientSection.ElementInformation.Properties[string.Empty].Value as ChannelEndpointElementCollection;
            bindingType = endpointCollection[0].Name;
            _client = new CarServiceClient(bindingType);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Car car;
           // if(_client.State == CommunicationState.Faulted) { _client = new CarServiceClient(bindingType); }

            try
            {
                 car = _client.GetCar(int.Parse(idTb.Text));
            }
            catch(FaultException ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                vendorTb.Text = string.Empty;
                 modelTb.Text = string.Empty;
                mileageTb.Text = string.Empty;
                YearTb.Text = string.Empty;
                return;
            }
            
            vendorTb.Text = car.Vendor;
            modelTb.Text = car.Model;
            mileageTb.Text = car.Mileage.ToString() ;
            YearTb.Text = car.Year.ToString("yyyy");
        }
    }
}
